class CreateBillDetails < ActiveRecord::Migration
  def change
    create_table :bill_details do |t|
      t.integer :quantity
      t.integer :totalPrice
      t.references :product, index: true, foreign_key: true
      t.references :bill, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
