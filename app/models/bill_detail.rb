class BillDetail < ActiveRecord::Base

  belongs_to :product
  belongs_to :bill

  before_save :get_total
  after_save  :get_total_for_bill

  def get_total

    @product    = Product.find(product_id)
    self.totalPrice =  @product.price * self.quantity
    self.bill_id = self.bill.id
  
  end

  def get_total_for_bill
    
    self.bill.subtotal = self.totalPrice + self.bill.subtotal
    self.bill.tax      = ((self.bill.subtotal * 12) / 100)
    self.bill.total    = self.bill.subtotal + self.bill.tax
    self.bill.save

    self.product.stock = self.product.stock - self.quantity 
    self.product.save

  end

end
