class Bill < ActiveRecord::Base
  
  belongs_to :user
  has_many :bill_details
  
  before_save   :saveUserInfo
  before_create :defaul_numbers

  def defaul_numbers
    self.subtotal  = 0
    self.tax = 0
    self. total = 0  
  end


  def saveUserInfo
    self.date = self.created_at  
  end

end
