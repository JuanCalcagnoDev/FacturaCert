class Product < ActiveRecord::Base
  
  has_many :bill_details
  has_many :bills, through: :bill_details
  
end
