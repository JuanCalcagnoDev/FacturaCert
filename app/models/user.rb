class User < ActiveRecord::Base

	has_many :bills
	has_many :bill_details, through: :bills

  validates :email                , presence: true , uniqueness: true
  validates :password             , presence: true

end
